import java.util.Scanner;
import java.util.ArrayList;
public class TicTacToeGame{
	public static void main(String[]args){
		System.out.println("Welcome to TicTacToe game!\nRule: 2 Players will alternate placing letter in a 3x3 board.\nIf Someone has connected 3 squares in a row (horizontally, vertically or diagonally), he/she wins.\nIf all the square are full and no one wins, it's a tie.");
		ArrayList<Board> boards=new ArrayList<Board>();
		boolean programOver=false;
		boolean gameOver=false;
		Scanner reader=new Scanner(System.in);
		int player=1;
		Square playerToken=Square.X;
		int[] numWinOfPlayer=new int[2];
		int numGame=0;
		while(programOver!=true){
			gameOver=false;
			boards.add(new Board());
			while(gameOver!=true){
				System.out.println(boards.get(numGame));
				if(player==1){
					playerToken=Square.X;
				}else{
					playerToken=Square.O;
				}
				
				System.out.println("P"+player+" is choosing place now. \n Please enter a number between 0-2(include) for the row and the column\nthat you want to choose.");
				int row=reader.nextInt();
				int col=reader.nextInt();
				boolean isTokenValid=boards.get(numGame).placeToken(row,col,playerToken);
				while(isTokenValid!=true){
					row=reader.nextInt();
					col=reader.nextInt();
					isTokenValid=boards.get(numGame).placeToken(row,col,playerToken);
					System.out.println("The value of the row or column that you choose is invalid.\nPlease try again.");	
				}
				
				if(boards.get(numGame).checkIfFull()==true){
					gameOver=true;
					System.out.println("It's a tie!");
				}
				if(boards.get(numGame).checkIfWinning(playerToken)==true){
					System.out.println("Player "+player+" is the winner.");
					numWinOfPlayer[player-1]++;
					gameOver=true;
				}else{
					player++;
					if(player>2){
						player=1;
					}
				}
			}
			numGame++;
			System.out.println("Do you want to start a new game?\nType yes or no to answer the question");
			String answer=reader.next();
			if(answer.equals("no")==true){
				programOver=true;
			}
		}
		System.out.println("Player 1 wins "+numWinOfPlayer[0]+" times.\nPlayer 2 wins "+numWinOfPlayer[1]+" times.");
	}
}
