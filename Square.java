public enum Square{
		X,
		O,
		BLANK;
		public String toString(){
			switch(this){
				default:return name();
				case BLANK:return "_";
			}
		}
	}