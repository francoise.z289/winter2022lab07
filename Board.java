public class Board{
	private Square[][] tictactoeBoard;
	public Board(){
		this.tictactoeBoard=new Square[][]{
			{Square.BLANK,Square.BLANK,Square.BLANK},
			{Square.BLANK,Square.BLANK,Square.BLANK},
			{Square.BLANK,Square.BLANK,Square.BLANK}
		};
	}
	public String toString(){
		for(Square[] row:this.tictactoeBoard){
			for(Square column: row){
				System.out.print(column);
			}
			System.out.println("");
		}
		return "";
	}
	public boolean placeToken(int row, int col, Square playerToken){
		boolean isValid=false;
		if((row>=0&&row<3)&&(col>=0&&col<3)&&this.tictactoeBoard[row][col].equals(Square.BLANK)){
			isValid=true;
			tictactoeBoard[row][col]=playerToken;
		}
		return isValid;
	}
	public boolean checkIfFull(){
		boolean isFull=false;
		int tokenCounter=0;
		for(Square[] row:this.tictactoeBoard){
			for(Square column: row){
				if(!column.equals(Square.BLANK)){
					//if the place is not blank(have X or O)
					tokenCounter++;
				}
			}
		}
		if(tokenCounter==9){
			isFull=true;
		}
		return isFull;
	}
	
	public boolean checkIfWinningHorizonal(Square playerToken){
		boolean isWinning=false;
		for(Square[] row:this.tictactoeBoard){
			int tokenCounter=0;
			//check three rows
			for(Square col: row){
				//check one row
				if(col.equals(playerToken)){
					tokenCounter++;
				}
			}
			if(tokenCounter==3){
				isWinning=true;
				System.out.println(playerToken+" win the game!");
			}
		}
		return isWinning;
	}
	public boolean checkWinningVertical(Square playerToken){
		boolean isWinning=false;
		for(int col=0; col<this.tictactoeBoard.length; col++){
			int tokenCounter=0;
			for(int row=0; row<this.tictactoeBoard.length;row++){
				if(this.tictactoeBoard[row][col].equals(playerToken)){
					tokenCounter++;
				}
			}
			if(tokenCounter==3){
				isWinning=true;
			}
		}
		return isWinning;
	}
	
	public boolean checkIfWinning(Square playerToken){
		boolean isWinning=false;
		if(checkIfWinningHorizonal(playerToken)||checkWinningVertical(playerToken)||checkIfWinningDiagonal(playerToken)){
			isWinning=true;
		}
		return isWinning;
	}
	
	public boolean checkIfWinningDiagonal(Square playerToken){
		boolean isWinning=false;
		if(checkIfWinningDiagonal1(playerToken)||checkIfWinningDiagonal2(playerToken)){
			isWinning=true;
		}
		return isWinning;
	}
	private boolean checkIfWinningDiagonal1(Square playerToken){
		/*Situation 1
			  012
			0 O__
			1 _O_
			2 __O
		*/
		int tokenCounter=0;
		boolean isWinning=false;
		if(tictactoeBoard[1][1].equals(playerToken)){
			if(tictactoeBoard[0][0].equals(playerToken)&&tictactoeBoard[2][2].equals(playerToken)){
				isWinning=true;
			}
		}	
		return isWinning;
	}
	private boolean checkIfWinningDiagonal2(Square playerToken){
		/*Situation 2
			  012
			0 __O
			1 _O_
			2 O__
		*/
		boolean isWinning=false;
		if(tictactoeBoard[1][1].equals(playerToken)){
			if(tictactoeBoard[0][2].equals(playerToken)&&tictactoeBoard[2][0].equals(playerToken)){
				isWinning=true;
			}
		}
		return isWinning;
	}
}

